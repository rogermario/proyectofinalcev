﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Inicio : MonoBehaviour {

	// Use this for initialization
	void Start () {

        SceneManager.LoadSceneAsync(0, LoadSceneMode.Additive);

        SceneManager.MoveGameObjectToScene(GameObject.FindGameObjectWithTag("Player"), SceneManager.GetSceneByBuildIndex(0));

        SceneManager.MoveGameObjectToScene(GameObject.FindGameObjectWithTag("MainCamera"), SceneManager.GetSceneByBuildIndex(0));
        
        Destroy(this);

    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
