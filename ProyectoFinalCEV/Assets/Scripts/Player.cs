﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour {

    #region Variables

    [Header("Control del Jugador")]

    public float velMovimiento;

    public float velSalto;

    public float distSueloSalto;

    public int ataqueDisparo;

    public float tiempoMinimoEntreDisparos;

    public float cadenciaAtaqueFisico;

    public int ataqueFisico;

    [Space]

    [Header("Referencias Necesarias")]

    public GameObject puntoDisparo;

    //Referencias Privadas

    private Rigidbody rigidPlayer;   

    private ControladorInstanciaciones ctrInstanciaciones;

    //Variables Privadas de Control

    private bool puedeDisparar = false;

    private float posicionDisparador;

    private float tiempoActualEntreDisparos = 10;

    private bool derecha = true;

    private bool escalera = false;

    private bool movimientoLateral = true;

    #endregion

    // Use this for initialization
    void Start () {

        rigidPlayer = this.GetComponent<Rigidbody>();        

        posicionDisparador = puntoDisparo.transform.localPosition.x;

        StartCoroutine(CogerControlador());
	}
	
	// Update is called once per frame
	void FixedUpdate () {

        MovimientoPlayer();

        Disparo();

    }

    IEnumerator CogerControlador()
    {
        yield return new WaitForSeconds(.5f);

        ctrInstanciaciones = GameObject.FindGameObjectWithTag("CtrInstanciaciones").GetComponent<ControladorInstanciaciones>();

        SceneManager.UnloadSceneAsync(2);
    }

    #region Controles

    void MovimientoPlayer()
    {
        if (Input.GetKey(KeyCode.D))
        {
            if (movimientoLateral)
            {
                this.transform.Translate(new Vector3(velMovimiento, 0, 0) * Time.deltaTime);

                derecha = true;

                puntoDisparo.transform.localPosition = new Vector3(posicionDisparador, puntoDisparo.transform.localPosition.y, puntoDisparo.transform.localPosition.z);
            }                       
        }

        if (Input.GetKey(KeyCode.A))
        {
            if (movimientoLateral)
            {
                this.transform.Translate(new Vector3(-velMovimiento, 0, 0) * Time.deltaTime);

                derecha = false;

                puntoDisparo.transform.localPosition = new Vector3(-posicionDisparador, puntoDisparo.transform.localPosition.y, puntoDisparo.transform.localPosition.z);
            }
            
        }

        if (Input.GetKeyDown(KeyCode.W))
        {
            if (!escalera)
            {
                RaycastHit miRaycast;

                if (Physics.Raycast(this.transform.position, -this.transform.up, out miRaycast, distSueloSalto))
                {
                    if (miRaycast.transform.tag == "Escenario")
                    {
                        rigidPlayer.velocity = Vector3.up * velSalto;
                    }
                }
            }           
            
        }

        if (Input.GetKey(KeyCode.W))
        {
            if (escalera)
            {
                transform.Translate(Vector3.up * .2f);
            }
        }

        if (Input.GetKey(KeyCode.S))
        {
            if (escalera)
            {
                transform.Translate(Vector3.down * .2f);
            }
        }

    }

    void Disparo()
    {
        if (!puedeDisparar)
        {
            tiempoActualEntreDisparos += Time.deltaTime;

            if (tiempoActualEntreDisparos >= tiempoMinimoEntreDisparos)
            {
                puedeDisparar = true;

                tiempoActualEntreDisparos = 0;
            }
        }        

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            if (puedeDisparar)
            {
                ctrInstanciaciones.Instanciar(1, puntoDisparo, derecha, true, 0, ataqueDisparo);

                puedeDisparar = false;
            }   
                      
        }

        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            if (puedeDisparar)
            {
                ctrInstanciaciones.Instanciar(2, puntoDisparo, derecha, true, cadenciaAtaqueFisico, ataqueFisico);

                puedeDisparar = false;
            }

        }

    }

    #endregion

    #region Collisiones

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Escaleras")
        {
            escalera = true;

            rigidPlayer.useGravity = false;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Escaleras")
        {
            escalera = false;

            rigidPlayer.useGravity = true;
        }
    }

    #endregion

}
