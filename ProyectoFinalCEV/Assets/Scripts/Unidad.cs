﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unidad : MonoBehaviour {

    #region Variables

    public bool player = false;

    [Space]

    [Header("Parametros Vida")]

    public int vida;

    public int vidaMaxima;

    private int vidaMaximaInicial;

    [Space]

    [Header("Parametros Escudos")]

    public bool escudo;

    public int vidaEscudo;

    public int vidaEscudoMaxima;    

    private int vidaEscudoMaximaInicial;

    #endregion

    // Use this for initialization
    void Start () {

        vidaMaximaInicial = vidaMaxima;

        vidaEscudoMaximaInicial = vidaEscudoMaxima;

	}
	
	// Update is called once per frame
	void Update () {

        if (vida < 0)
        {
            vida = 0;
        }

        if (vidaEscudo < 0)
        {
            vidaEscudo = 0;
        }

        if (vida > vidaMaxima)
        {
            vida = vidaMaxima;
        }

        if (vidaEscudo > vidaEscudoMaxima)
        {
            vidaEscudo = vidaEscudoMaxima;
        }

        if (vida <= 0)
        {
            Destroy(this.gameObject);
        }

    }

    public void RestarVida(int cantidad)
    {
        if (escudo && vidaEscudo != 0)
        {
            vidaEscudo -= cantidad;
        }        
        else
        {
            vida -= cantidad;
        }

    }

    public void SumarVida(int cantidad)
    {
        vida += cantidad;
    }

    public void RegenerarEscudo(int cantidad)
    {
        vidaEscudo += cantidad;
    }

}
