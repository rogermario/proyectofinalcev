﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CargarEscena : MonoBehaviour
{

    public int escena;

    public bool eliminarEscena = false;

    private int escenaBuena;

    // Use this for initialization
    void Start()
    {
        if (escena == 1)
        {
            escenaBuena = 0;
        }
        else
        {
            escenaBuena = 1;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    void ManejarEscena()
    {
        if (eliminarEscena)
        {
            DeleteScene();
        }
        else
        {
            LoadScene();
        }
    }

    void LoadScene()
    {
        if (SceneManager.sceneCount < 2)
        {
            SceneManager.LoadSceneAsync(escena, LoadSceneMode.Additive);
        }
    }

    void DeleteScene()
    {
        if (SceneManager.sceneCount > 1)
        {
            SceneManager.MoveGameObjectToScene(GameObject.FindGameObjectWithTag("Player"), SceneManager.GetSceneByBuildIndex(escenaBuena));

            SceneManager.MoveGameObjectToScene(GameObject.FindGameObjectWithTag("MainCamera"), SceneManager.GetSceneByBuildIndex(escenaBuena));

            SceneManager.UnloadSceneAsync(escena);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            ManejarEscena();
        }
    }

}
    