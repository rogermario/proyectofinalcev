﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorAtaques : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Atacar(int tipoAtaque, int cantidad, Unidad unidadEnemiga)
    {
        switch (tipoAtaque)
        {
            case 1:

                unidadEnemiga.RestarVida(cantidad);

                break;
        }
                
    }
}
