﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camara : MonoBehaviour {

    private GameObject player;

    private Vector3 distancia;

	// Use this for initialization
	void Start () {

        player = GameObject.FindGameObjectWithTag("Player");

        distancia = this.transform.position - player.transform.position;

	}
	
	// Update is called once per frame
	void Update () {

        this.transform.position = player.transform.position + distancia;

	}
}
