﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Proyectil : MonoBehaviour {

    [HideInInspector]
    public bool derecha;

    [HideInInspector]
    public float ataque;

    public float vel;

    [HideInInspector]
    public bool player;

    private float turbulencia;

    private float tiempoActual;

    private bool positivo;

    private ControladorAtaques ctrAtaques;

	// Use this for initialization
	void Start () {

        turbulencia = Random.Range(-1f, 1f);

        ctrAtaques = GameObject.FindGameObjectWithTag("CtrAtaques").GetComponent<ControladorAtaques>();

	}
	
	// Update is called once per frame
	void FixedUpdate () {

        Moverse();

        Turbulencias();

    }

    public void Moverse()
    {
        if (derecha)
        {
            this.transform.Translate(vel * Time.deltaTime, 0, 0);
        }
        else        
        {
            this.transform.Translate(-vel * Time.deltaTime, 0, 0);
        }
    }

    void Turbulencias()
    {
        if (tiempoActual < .5f)
        {
            tiempoActual += Time.deltaTime;
        }
        else
        {
            positivo = !positivo;

            tiempoActual = 0;
        }

        if (positivo)
        {
            this.transform.Translate(0, turbulencia * Time.deltaTime, 0);
        }

        else
        {
            this.transform.Translate(0, -turbulencia * Time.deltaTime, 0);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (player)
        {
            if (other.tag == "Enemy")
            {
                ctrAtaques.Atacar(1, 10, other.GetComponent<Unidad>());

                Destroy(this.gameObject);
            }
        }  
        else
        {
            if (other.tag == "Player")
            {
                ctrAtaques.Atacar(1, 10, other.GetComponent<Unidad>());

                Destroy(this.gameObject);
            }
        }

        if (other.tag == "Superficie")
        {
            Destroy(this.gameObject);
        }
    }
}
