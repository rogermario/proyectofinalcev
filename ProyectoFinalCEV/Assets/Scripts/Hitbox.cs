﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hitbox : MonoBehaviour {

    #region Variables

    [HideInInspector]
    public float cadenciaAtaque;

    [HideInInspector]
    public int ataque;

    private float tiempo = 0;

    private float tiempoMuerte = 1;

    private List<GameObject> enemigosAtacados = new List<GameObject>();

    private ControladorAtaques ctrAtaques;

    [HideInInspector]
    public bool player;

    #endregion

    // Use this for initialization
    void Start () {

        ctrAtaques = GameObject.FindGameObjectWithTag("CtrAtaques").GetComponent<ControladorAtaques>();

    }
	
	// Update is called once per frame
	void Update () {

        AutoDestruccion();

    }

    void AutoDestruccion()
    {
        tiempo += Time.deltaTime;

        if (tiempo >= tiempoMuerte)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy" && player)
        {
            bool atacar = true;

            if (enemigosAtacados.Count != 0)
            {
                foreach (GameObject enemigoYaAtacado in enemigosAtacados)
                {
                    if (other.gameObject == enemigoYaAtacado)
                    {
                        atacar = false;
                    }
                }
            }            

            enemigosAtacados.Add(other.gameObject);

            if (atacar)
            {
                Unidad miUnidad = other.GetComponent<Unidad>();

                StartCoroutine(EnviarAtaque(miUnidad));
            }
        }

        if (other.tag == "Player" && !player)
        {
            bool atacar = true;

            if (enemigosAtacados.Count != 0)
            {
                foreach (GameObject enemigoYaAtacado in enemigosAtacados)
                {
                    if (other.gameObject == enemigoYaAtacado)
                    {
                        atacar = false;
                    }
                }
            }

            enemigosAtacados.Add(other.gameObject);

            if (atacar)
            {
                Unidad miUnidad = other.GetComponent<Unidad>();

                StartCoroutine(EnviarAtaque(miUnidad));
            }
        }
    }

    IEnumerator EnviarAtaque(Unidad enemigo)
    {
        yield return new WaitForSeconds(cadenciaAtaque);

        ctrAtaques.Atacar(1, ataque, enemigo);
    }
}
