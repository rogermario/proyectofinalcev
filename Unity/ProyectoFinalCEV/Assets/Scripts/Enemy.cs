﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    #region Variables

    public GameObject puntoDisparo;

    public int ataque;
        
    public bool ataqueCuerpo;

    public int ataqueFisico;

    private bool derecha = false;

    private float tiempo = 0;

    private float tiempoMaximo;

    private float tiempoSalto = 0;

    private float tiempoMaximoSalto;

    private ControladorInstanciaciones ctrInstanciaciones;

    private Rigidbody rigidEnemy;

    #endregion

    // Use this for initialization
    void Start () {

        tiempoMaximo = Random.Range(1f, 5f);

        tiempoMaximoSalto = Random.Range(1f, 5f);

        ctrInstanciaciones = GameObject.FindGameObjectWithTag("CtrInstanciaciones").GetComponent<ControladorInstanciaciones>();

        rigidEnemy = this.GetComponent<Rigidbody>();

    }
	
	// Update is called once per frame
	void Update () {

        tiempo += Time.deltaTime;

        tiempoSalto += Time.deltaTime;

        if (tiempo >= tiempoMaximo)
        {
            if (ataqueCuerpo)
            {
                AtaqueFisico();
            }
            else
            {
                Disparar();
            }            
        }

        if (tiempoSalto >= tiempoMaximoSalto)
        {
            Saltar();
        }

    }

    void Disparar()
    {        
            ctrInstanciaciones.Instanciar(1, puntoDisparo, derecha, false, 0, ataque);            

            tiempoMaximo = Random.Range(1f,5f);

            tiempo = 0;        
    }

    void Saltar()
    {
        RaycastHit miRaycast;

        if (Physics.Raycast(this.transform.position, -this.transform.up, out miRaycast, 1))
        {
            if (miRaycast.transform.tag == "Escenario")
            {
                rigidEnemy.velocity = Vector3.up * 8;
            }
        }        

        tiempoMaximoSalto = Random.Range(1f, 5f);

        tiempoSalto = 0;
    }

    void AtaqueFisico()
    {
        ctrInstanciaciones.Instanciar(2, puntoDisparo, derecha, false, 0, ataqueFisico);

        tiempoMaximo = Random.Range(1f, 5f);

        tiempo = 0;
    }
}
