﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorInstanciaciones : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Instanciar(int tipo, GameObject pos, bool derecha, bool player, float tiempo, int ataque)
    {
        switch (tipo)
        {
            case 1:

                GameObject bala = Instantiate(Resources.Load("Bala"), pos.transform.position, pos.transform.rotation) as GameObject;

                Proyectil scriptBala = bala.GetComponent<Proyectil>();

                scriptBala.derecha = derecha;

                scriptBala.player = player;

                scriptBala.ataque = ataque;

                break;

            case 2:

                GameObject hitbox = Instantiate(Resources.Load("Hitbox"), new Vector3(pos.transform.position.x, pos.transform.position.y - 2.6f, pos.transform.position.z), pos.transform.rotation) as GameObject;

                Hitbox miHitbox = hitbox.GetComponent<Hitbox>();

                miHitbox.cadenciaAtaque = tiempo;

                miHitbox.ataque = ataque;

                miHitbox.player = player;

                break;

        }
    }
}
